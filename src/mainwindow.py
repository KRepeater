#!/usr/bin/env python

from PyQt4.QtCore import SIGNAL,SLOT,Qt
from PyQt4.QtCore import QPoint
from PyQt4.QtCore import QString
from PyQt4.QtCore import QSettings
from PyQt4.QtCore import QDir
from PyQt4.QtCore import QCoreApplication

from PyQt4.QtGui import QWidget
from PyQt4.QtGui import QLabel
from PyQt4.QtGui import QMainWindow
from PyQt4.QtGui import QSlider
from PyQt4.QtGui import QFileDialog
from PyQt4.QtGui import QIcon

from mprocess      import MProcess
from ui_mainwindow import Ui_MainWindow

class MainWindow(QMainWindow,Ui_MainWindow):
    def __init__(self, parent = None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        
        ##Set MainWindow's style
        self.setStyleSheet('MainWindow {background-image:url(:/background.png);}')

        ##Quit if close this window
        self.setAttribute(Qt.WA_QuitOnClose)
        self.settings = QSettings(QDir.home().absolutePath().append(QDir.separator()) + ".KRepeater",
                                  QSettings.NativeFormat,self)
        self.currDir = QDir(self.settings.value("LastPath").toString())

        self.statusLabel = QLabel()
        self.statusBar.addWidget(self.statusLabel)

        ##Set up the volume slide widget for setting volume
        self.volumeSlide = QSlider(self)
        self.volumeSlide.hide()
        self.volumeSlide.setFocusPolicy(Qt.NoFocus)
        self.volumeSlide.resize(15,110)
        self.volumeSlide.setRange(0,100)
        self.volumeSlide.setValue(60)   # Write to config later

        #Create a mprocess object to control mplayer process
        self.mprocess = MProcess(self.videoWidget.winId(),self)

        self.mprocess.setSlider(self.slider)

        self.connect(self.pauseBtn, SIGNAL("clicked()"),
                     self.pause)

        self.connect(self.stopBtn, SIGNAL("clicked()"),
                     self.stopped)

        self.connect(self.stopBtn, SIGNAL("clicked()"),
                     self.mprocess.stop)

        self.connect(self.volumeBtn, SIGNAL("toggled(bool)"),
                     self.volumeBtnToggled)

        self.connect(self.volumeSlide, SIGNAL("valueChanged(int)"),
                     self.mprocess.setVolume)

        self.connect(self.openAction,SIGNAL("triggered()"),
                     self.openFile)

        self.connect(self.openBtn,SIGNAL("clicked()"),
                     self.openFile)

        self.connect(self.mprocess,SIGNAL("updateStatus()"),
                     self.setStatusInfo)

        self.connect(self.aBtn, SIGNAL("toggled(bool)"),self.pointA)
        self.connect(self.bBtn, SIGNAL("toggled(bool)"),self.pointB)

    #END def __init__(self, parent = None)

    def showVolumeSlide(self):
        globalPoint = self.btnWidget.mapToGlobal(self.volumeBtn.pos())
        newPoint = self.mapFromGlobal(globalPoint) \
                + QPoint(self.volumeBtn.width(),
                         self.volumeBtn.height() - self.volumeSlide.height())
        self.volumeSlide.move(newPoint)
        self.volumeSlide.show()
    #END def showVolumeSlide(self)

    def volumeBtnToggled(self,checked):
        if(checked == True):
            self.volumeBtn.setChecked(True)
            self.showVolumeSlide()
        else:
            self.volumeBtn.setChecked(False)
            self.volumeSlide.hide()
        #END def volumeBtnToggled(self,checked):

    def mouseReleaseEvent(self,event):
        self.volumeBtnToggled(False)
    #END  def mouseReleasedEvent(self,event)

    def openFile(self):
        fileName = str(QFileDialog.getOpenFileName(self,
                                                   "Video File",
                                                   self.currDir.path()))
        if(len(fileName) == 0):
            return

        n = fileName.rfind(str(QDir.separator().toAscii()))
        self.currDir.setPath(fileName[0:n])
        self.settings.setValue("LastPath",self.currDir.path())
        
        self.mprocess.setFileName(fileName)
        self.play()
    #END def openFile(self)
    
    def play(self):
        self.mprocess.play()
        self.pauseBtn.setEnabled(True)
        self.stopBtn.setEnabled(True)
        self.aBtn.setEnabled(True)
        self.pauseBtn.setIcon(QIcon(":/pause.png"))
    #END  def play(self)

    def stopped(self):
        self.pauseBtn.setIcon(QIcon(":/play.png"))
        self.pauseBtn.setEnabled(False)
        self.stopBtn.setEnabled(False)
        self.slider.setValue(0)
        self.statusLabel.setText("")
        QCoreApplication.processEvents()
    #END def stopped(self):

    def stop(self):
        self.stopped()
        self.mprocess.stop()
    #END def stop(self)

    def pause(self):
        self.mprocess.pause()
        print "self.mprocess.playerState  ", self.mprocess.playerState
        if(self.mprocess.playerState == MProcess.PAUSED):
            self.pauseBtn.setIcon(QIcon(":/play.png"))
        else:
            self.pauseBtn.setIcon(QIcon(":/pause.png"))
    #END def pause(self):

    def pointA(self):
        if(self.aBtn.isChecked()):
            self.mprocess.setA()
            self.bBtn.setEnabled(True)
        else:
            self.mprocess.cleanPoints()
            self.bBtn.setChecked(False)
            self.bBtn.setEnabled(False)
    #END def pointA(self):

    def pointB(self):
        if(self.bBtn.isChecked()):
            self.mprocess.setB()
        else:
            self.mprocess.cleanPoints()
            self.aBtn.setChecked(False)
            self.bBtn.setChecked(False)

            self.aBtn.setEnabled(True)
            self.bBtn.setEnabled(False)
    #END def pointB(self):

    def setStatusInfo(self):
        colPre = '<font color="white">'
        colBak = '</font>'
        black = '&nbsp;' * 8
        if(self.mprocess.timeLength != None):
            message = str(int(self.mprocess.timePos)*100/int(self.mprocess.timeLength))
            message += "%"
        else:
            message = "0%"
        message += black
        if(self.mprocess.timePos != None):
            message += 'pos='
            message += str(self.mprocess.timePos)
            message += black

        if(self.mprocess.pa != None):
            message += "A=" + str(self.mprocess.pa)
            message += black
        if(self.mprocess.pb != None):
            message += "B=" + str(self.mprocess.pb)
            message += black
        self.statusLabel.setText(QString(colPre + message + colBak))
    #END def setStatusInfo(self, currProcess, pa, pb, status):

#END class MainWindow(QMainWindow,Ui_MainWindow)
