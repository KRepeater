#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys

class MainWindow(QWidget):
    def __init__(self, parent = None):
        super(MainWindow,self).__init__(parent)
        self.widget = QWidget(self)
        self.widget.setEnabled(False)
        self.widget.setFocusPolicy(Qt.NoFocus)

        self.lineEdit = QLineEdit(self)
        self.widget.setFocus()
        self.widget.setFocusPolicy(Qt.StrongFocus)

        self.btn = QPushButton("Excute",self)

        cmd = "mplayer"
        args = QStringList(["-slave","-quiet","-wid", str(self.widget.winId()),
                            "/backup/Movie/BraveHeart.rmvb"])

        self.process = QProcess()

        self.connect(self.btn, SIGNAL("clicked()"),self.excuteCmd)
        self.connect(self.process, SIGNAL("readyReadStandardOutput()"),
                     self.printOut)

        self.resize(300,240)
        self.process.start(cmd,args)



        #END def __init__(self, parent = None):

    def excuteCmd(self):
        self.process.write("get_time_pos\n")
        self.process.write((self.lineEdit.text() + "\n").toUtf8())
    #END def excuteCmd(self):

    def printOut(self):
        print self.process.readAll()
    #END     def printOut(self):

    def resizeEvent(self, event):
        self.widget.setGeometry(0,0,event.size().width(),
                                event.size().height()-32)
        self.lineEdit.setGeometry(0,self.widget.height(),100,32);
        self.btn.setGeometry(110,self.widget.height(),100,32);

    #END def resize(self, event):

    def keyPressEvent(self,event):
        pass
    #END def keyPressEvent(self,event):

#END class MainWindow(QWidget):


def main():
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(True)
    mainWidget = MainWindow()
    mainWidget.show()
    app.exec_()
#END def main():

if __name__ == '__main__':
    main()
