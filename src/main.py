#!/usr/bin/env python

import sys
from PyQt4.QtGui import QApplication
from mainwindow  import MainWindow

def main():
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(True)
    mainWindow = MainWindow()
    mainWindow.show()
    app.exec_()

if __name__ == '__main__':
    main()
