#!/usr/bin/env python

from PyQt4.QtCore import SIGNAL,SLOT,Qt

from PyQt4.QtCore import QProcess
from PyQt4.QtCore import QStringList

import re

class MProcess(QProcess):
    """ Mplayer process controller
        Signals:   stopped()
    """

    STOPPED, PAUSED, PLAYING = range(3)
    INTERVAL = 200 #For check timer

    def __init__(self, wid = None, parent = None):
        super(MProcess, self).__init__(parent)
        self.playerState = MProcess.STOPPED
        ##The current file's name.
        self.fileName = None

        ##Slider should be a QSlider or its subclass-object
        ##self.slider will be update it isn't None
        self.slider = None

        ##The Widget Id which used to play vedio.
        self.wid = wid

        ##Point A and Point B used to circle
        self.pa  = None;
        self.pb  = None;

        self.timerId = self.startTimer(MProcess.INTERVAL)

        self.timePos = None
        self.timeLength = None

        self.connect(self, SIGNAL("readyReadStandardOutput()"),
                     self.analyseResult)
        self.connect(self, SIGNAL("readyReadStandardError()"),
                     self.analyseResult)

    #END def __init__(self, parent = None):

    def setSlider(self,slider):
        self.slider = slider
        self.connect(self.slider,SIGNAL("sliderPressed()"),
                     self.sliderPressed)
        self.connect(self.slider,SIGNAL("sliderReleased()"),
                     self.sliderReleased)
    #END def setSlider(self,slider):

    def sliderPressed(self):
        if(self.playerState == MProcess.PLAYING):
            self.pause()
    #END def sliderPressed(self):
    
    def sliderReleased(self):
        self.timePos = self.slider.value()
        self.write("seek "+ str(self.slider.value()) + " 2\n")
        self.pause()
    #END def sliderReleased(self):

    def setFileName(self,fileName):
        self.fileName = fileName

    #END def setFileName(self,fileName):

    def play(self):
        if(self.fileName == None):
            return

        cmd = "mplayer"
        args = QStringList(["-slave",
                            "-quiet",
                            "-wid", str(self.wid), self.fileName])

        if(self.state() != QProcess.NotRunning):
            self.terminate()
            if(not self.waitForFinished()):
                self.kill()
        self.timeLength = None


        self.playerState = MProcess.PLAYING
        self.start(cmd, args)
        self.setupTimer()
    #END def play(self)

    def setA(self):
        self.pa  = int(self.timePos)/5*5;

    def setB(self):
        self.pb  = int(self.timePos);

    def cleanPoints(self):
        self.pa = None
        self.pb = None

    #END def setCirclePoint(self):

    def setVolume(self,value):
        if(self.playerState == MProcess.PLAYING):
            self.write("volume " + str(value) + " 1\n")
        else:
            pass
    #END def setVolume(self,value):

    def setupTimer(self):
        if(self.timerId == None):
            self.timerId = self.startTimer(MProcess.INTERVAL)
        else:
            pass
    #END def setupTimer(self):

    def closeTimer(self):
        if(self.timerId != None):
            self.killTimer(self.timerId)
            self.timerId = None
        else:
            pass
    #END def closeTimer(self):

    def pause(self):
        if(self.playerState == MProcess.PAUSED):
            self.playerState = MProcess.PLAYING
            self.setupTimer()
        elif(self.playerState == MProcess.PLAYING):
            self.closeTimer()
            self.playerState = MProcess.PAUSED
        else:  ## if STOPPED, then return directly
            self.closeTimer()
            return
        self.write("pause \n")
    #END def pause(self):

    def stop(self):
        self.closeTimer()
        self.playerState == MProcess.STOPPED
        self.timeLength = None
        self.write("stop \n")
    #END  def stop(self):

    def analyseResult(self):
        resStr = self.readAll().data()
        if(re.search("POSITION",resStr)):
            self.timePos = float((re.search("\d+\.?\d",resStr).group(0)))
        elif(re.search("LENGTH",resStr)):
            self.timeLength = float(re.search("\d+\.?\d",resStr).group(0))
            if(self.slider != None):
                self.slider.setRange(0,int(self.timeLength)+1)
        elif(re.search("End of file",resStr)):
            self.closeTimer()
            self.playerState = MProcess.STOPPED
            self.emit(SIGNAL("stopped()"))
        else:
            print resStr



        if(self.pa == None or self.pb == None):
            return

        if( self.pa >= self.pb ):
            self.pb += 10

        if(self.timePos > self.pb):
            self.timePos = self.pa
            self.write("seek "+ str(self.pa) + " 2\n")

    #END  def analyseResult(self):

    def timerEvent(self,event):
        if(event.timerId() != self.timerId):
            return

        print self.timePos, "  ", self.pa, "  ", self.pb
        if(self.slider != None and self.timePos != None):
            self.slider.setValue(int(self.timePos))

        if(self.playerState == MProcess.PAUSED):
            self.closeTimer()
            return

        if(self.state() == QProcess.NotRunning):
            self.closeTimer()
            self.playerState = MProcess.STOPPED
            return

        self.write("get_time_pos\n")

        if(self.timeLength == None):
            self.write("get_time_length\n")
        else:
            self.emit(SIGNAL("updateStatus()"))
    #END def timerEvent(self,event):

#END class MProcess(QProcess):
